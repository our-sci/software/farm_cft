<?php

/**
 * @file
 * Provide Views data for farm_cft.module.
 */

/**
 * Implements hook_views_data().
 */
function farm_cft_views_data() {

  // Build array of views data.
  $data = [];

  // Base CFT assessment table.
  $data['farm_cft_assessment'] = [
    'table' => [
      'group' => t('CFT Assessments'),
      'base' => [
        'title' => t('CFT Assessments'),
        'help' => t('Base table for all CFT assessments.'),
      ],
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('Unique ID of the base assessment.'),
      'field' => [
        'id' => 'numeric',
        'click sortable' => TRUE,
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ],
    'assessment_type' => [
      'title' => t('Assessment Type'),
      'help' => t('Assessment Type for the base assessment.'),
      'field' => [
        'id' => 'standard',
        'click sortable' => TRUE,
      ],
      'filter' => [
        'id' => 'standard',
      ],
    ],
    'asset_id' => [
      'title' => t('Asset ID'),
      'help' => t('ID of the location asset associated with the assessment.'),
      'relationship' => [
        'base' => 'asset_field_data',
        'base_field' => 'asset_id',
        'id' => 'standard',
        'label' => t('Location asset'),
      ],
    ],
    'timestamp' => [
      'title' => t('Assessment timestamp'),
      'help' => t('Timestamp the CFT assessment was taken.'),
      'field' => [
        'id' => 'date',
        'click sortable' => TRUE,
      ],
    ],
  ];

  $data['farm_cft_assessment_crop'] = [
    'table' => [
      'group' => t('CFT Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base CFT Assessment.'),
      'relationship' => [
        'base' => 'farm_cft_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'emissions_total' => [
      'title' => t('Emissions Total'),
      'help' => t('Emissions Total'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'emissions_per_area' => [
      'title' => t('Emissions Total Area'),
      'help' => t('Emissions Total Area'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'emissions_per_product' => [
      'title' => t('Emissions Total Product'),
      'help' => t('Emissions Total Product'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'soil_organic_carbon' => [
      'title' => t('Soil Organic Carbon'),
      'help' => t('Soil Organic Carbon'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'machinery_fuel_usage' => [
      'title' => t('Machinery Fuel Usage'),
      'help' => t('Machinery Fuel Usage'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_cft_assessment']['assessment_crop'] = [
    'title' => t('Assessment Crop Data'),
    'help' => t('CFT assessment crop'),
    'relationship' => [
      'label' => t('Assessment Crop'),
      'id' => 'standard',
      'base' => 'farm_cft_assessment_crop',
      'base field' => 'assessment_id',
      'field' => 'assessment_id',
    ],
  ];

  $data['farm_cft_assessment_total_emissions'] = [
    'table' => [
      'group' => t('CFT Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base CFT Assessment.'),
      'relationship' => [
        'base' => 'farm_cft_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'name' => [
      'title' => t('Name'),
      'help' => t('Emission Name'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'co2' => [
      'title' => t('CO2'),
      'help' => t('Emissions C02'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'n2o' => [
      'title' => t('N2O'),
      'help' => t('Emissions N20'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'ch4' => [
      'title' => t('CH4'),
      'help' => t('Emissions CH4'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'total_co2e' => [
      'title' => t('Total CO2E'),
      'help' => t('Emissions Total CO2E'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'total_co2e_per_area' => [
      'title' => t('Total CO2E Per Area'),
      'help' => t('Emissions Total CO2E Per Area'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'total_co2e_per_product' => [
      'title' => t('Total CO2E Per Product'),
      'help' => t('Emissions Total CO2E Per Product'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_cft_assessment']['farm_cft_assessment_total_emissions'] = [
    'title' => t('Assessment Crop Total Emissions Data'),
    'help' => t('CFT Assessment Crop Total Emissions'),
    'relationship' => [
      'label' => t('Assessment Crop Total Emissions'),
      'id' => 'standard',
      'base' => 'farm_cft_assessment_total_emissions',
      'base field' => 'assessment_id',
      'field' => 'assessment_id',
    ],
  ];

  $data['farm_cft_assessment_water'] = [
    'table' => [
      'group' => t('CFT Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base CFT Assessment.'),
      'relationship' => [
        'base' => 'farm_cft_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'water_productivity' => [
      'title' => t('Water Productivity'),
      'help' => t('Water Productivity'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'water_footprint_total' => [
      'title' => t('Water footprint Total'),
      'help' => t('Water footprint Total'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'water_footprint_green' => [
      'title' => t('Water footprint Green'),
      'help' => t('Water footprint Green'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'water_footprint_blue' => [
      'title' => t('Water footprint Blue'),
      'help' => t('Water footprint Blue'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'irrigation_efficiency' => [
      'title' => t('Irrigation Efficiency'),
      'help' => t('Irrigation Efficiency'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'water_balance_unit' => [
      'title' => t('Water Balance Unit'),
      'help' => t('Water Balance Unit'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'water_balance_water_requirement' => [
      'title' => t('Water Requirement'),
      'help' => t('Water Requirement'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'water_balance_water_added' => [
      'title' => t('Water Balance'),
      'help' => t('Water Balance'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'irrigation_percolation' => [
      'title' => t('Irrigation Percolation'),
      'help' => t('Irrigation Percolation'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'irrigation_runoff' => [
      'title' => t('Irrigation Runoff'),
      'help' => t('Irrigation Runoff'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'rainfall_runoff' => [
      'title' => t('Rainfall Runoff'),
      'help' => t('Rainfall Runoff'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'rainfall_percolation' => [
      'title' => t('Rainfall Percolation'),
      'help' => t('Rainfall Percolation'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'irrigation_balance' => [
      'title' => t('Irrigation Balance'),
      'help' => t('Irrigation Balance'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'interception_losses' => [
      'title' => t('Interception Losses'),
      'help' => t('Interception Losses'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_cft_assessment']['assessment_water'] = [
    'title' => t('Assessment Water Data'),
    'help' => t('CFT Assessment Water'),
    'relationship' => [
      'label' => t('Assessment Water'),
      'id' => 'standard',
      'base' => 'farm_cft_assessment_water',
      'base field' => 'assessment_id',
      'field' => 'assessment_id',
    ],
  ];

  $data['farm_cft_assessment_dairy'] = [
    'table' => [
      'group' => t('CFT Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base CFT Assessment.'),
      'relationship' => [
        'base' => 'farm_cft_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'emissions_total' => [
      'title' => t('Emissions Total'),
      'help' => t('Emissions Total'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'emissions_per_fpcm' => [
      'title' => t('Emissions Per FPCM'),
      'help' => t('Emissions Per FPCM'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_cft_assessment_total_emissions_dairy'] = [
    'table' => [
      'group' => t('CFT Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base CFT Assessment.'),
      'relationship' => [
        'base' => 'farm_cft_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'name' => [
      'title' => t('Total Emissions Name'),
      'help' => t('Total Emissions Name'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'co2' => [
      'title' => t('Total Emissions CO2'),
      'help' => t('Total Emissions CO2'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'n2o' => [
      'title' => t('Total Emissions N2O'),
      'help' => t('Total Emissions N2O'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'ch4' => [
      'title' => t('Total Emissions CH4'),
      'help' => t('Total Emissions CH4'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'co2e' => [
      'title' => t('Total Emissions CO2E'),
      'help' => t('Total Emissions CO2E'),
      'field' => [
        'id' => 'standard',
      ],
    ],
    'co2e_per_fpcm' => [
      'title' => t('Total Emissions CO2E Per FPCM'),
      'help' => t('Total Emissions CO2E Per FPCM'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_cft_assessment']['assessment_diary'] = [
    'title' => t('Assessment Dairy Data'),
    'help' => t('CFT assessment dairy'),
    'relationship' => [
      'label' => t('Assessment Dairy'),
      'id' => 'standard',
      'base' => 'farm_cft_assessment_dairy',
      'base field' => 'assessment_id',
      'field' => 'assessment_id',
    ],
  ];

  $data['farm_cft_assessment_energy'] = [
    'table' => [
      'group' => t('CFT Assessments'),
    ],
    'assessment_id' => [
      'title' => t('Assessment ID'),
      'help' => t('ID of the base CFT Assessment.'),
      'relationship' => [
        'base' => 'farm_cft_assessment',
        'base_field' => 'assessment_id',
        'id' => 'standard',
        'label' => t('Base Assessment'),
      ],
    ],
    'energy' => [
      'title' => t('Assessment Energy'),
      'help' => t('Assessment Energy'),
      'field' => [
        'id' => 'standard',
      ],
    ],
  ];

  $data['farm_cft_assessment']['assessment_energy'] = [
    'title' => t('Assessment Energy Data'),
    'help' => t('CFT Assessment Energy'),
    'relationship' => [
      'label' => t('Assessment Energy'),
      'id' => 'standard',
      'base' => 'farm_cft_assessment_energy',
      'base field' => 'assessment_id',
      'field' => 'assessment_id',
    ],
  ];
  return $data;
}
