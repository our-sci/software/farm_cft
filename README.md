# farm_cft

farmOS integration with [Cool Farm Tool](https://coolfarmtool.org)

This module is an add-on for the [farmOS](http://drupal.org/project/farm)
distribution.

## Getting started

### Installation

Install as you would normally install a contributed drupal module.

TODO: Add composer install command.

### Configuration

TODO: Api key.

## Features

## FAQ

## Maintainers

Current maintainers:
- Paul Weidner [@paul121](https://github.com/paul121)

## Sponsors
