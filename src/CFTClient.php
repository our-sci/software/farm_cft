<?php

namespace Drupal\farm_cft;

use GuzzleHttp\Client;
use Drupal\Component\Serialization\Json;

/**
 * Extends the Guzzle HTTP client with helper methods for the CFT API.
 *
 * See https://app.coolfarmtool.org/api/v1/demo.
 */
class CFTClient extends Client implements CFTClientInterface {

  /**
   * The base URI of the CFT API.
   *
   * @var string
   */
  public static string $cftApiBaseUri = 'https://app.coolfarmtool.org/api/v1/';

  /**
   * CFTClient constructor.
   *
   * @param string $api_key
   *   The CFT API key.
   * @param string $api_app_key
   *   The CFT APP API key.
   * @param array $config
   *   Guzzle client config.
   */
  public function __construct(string $api_key, string $api_app_key, array $config = []) {
    $default_config = [
      'base_uri' => self::$cftApiBaseUri,
      'headers' => [
        'Content-Type' => 'application/json',
        'X-Api-Authorization' => $api_key,
        'X-Api-App-Authorization' => $api_app_key,
      ],
      'http_errors' => FALSE,
    ];
    $config = $default_config + $config;
    parent::__construct($config);
  }

  /**
   * Returns CFT Crop Product Calculate.
   */
  public function cropCalculate(array $assessment_data) {
    $res = $this->request('POST', 'crop_product/calculate/', ['json' => $assessment_data]);

    if ($res->getStatusCode() === 200) {
      $results = Json::decode((string) $res->getBody());
      return $results;
    }
    return $res->getStatusCode();
  }

  /**
   * Returns CFT Crop Product Water Calculate.
   */
  public function waterCalculate(array $assessment_data) {
    $res = $this->request('POST', 'crop_product/water/calculate/', ['json' => $assessment_data]);

    if ($res->getStatusCode() === 200) {
      $results = Json::decode((string) $res->getBody());
      return $results;
    }
    return $res->getStatusCode();
  }

  /**
   * Returns CFT Daily Product Calculate.
   */
  public function dairyCalculate(array $assessment_data) {
    $res = $this->request('POST', 'dairy_product/calculate/', ['json' => $assessment_data]);

    if ($res->getStatusCode() === 200) {
      $results = Json::decode((string) $res->getBody());
      return $results;
    }
    return $res->getStatusCode();
  }

  /**
   * Returns CFT Energy Calculate.
   */
  public function energyCalculate(array $assessment_data) {
    $res = $this->request('POST', 'energy/calculate/', ['json' => $assessment_data]);

    if ($res->getStatusCode() === 200) {
      $results = Json::decode((string) $res->getBody());
      return $results;
    }
    return $res->getStatusCode();
  }

  /**
   * {@inheritdoc}
   */
  public function ping(): bool {
    $res = $this->request('GET', '/ping');
    return $res->getStatusCode() === 200;
  }

}
