<?php

namespace Drupal\farm_cft;

use Drupal\asset\Entity\AssetInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;

/**
 * A service for interacting with CFT Assessment.
 */
class CFTAssessment implements CFTAssessmentInterface {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;


  /**
   * The CFT client service.
   *
   * @var \Drupal\farm_cft\CFTClientInterface
   */
  protected $cftClient;

  /**
   * Constructs the CFTAssessment class.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\farm_cft\CFTClientInterface $cft_client
   *   The CFT client service.
   */
  public function __construct(Connection $connection, TimeInterface $time, CFTClientInterface $cft_client) {
    $this->database = $connection;
    $this->time = $time;
    $this->cftClient = $cft_client;
  }

  /**
   * {@inheritdoc}
   */
  public function createAssessment(AssetInterface $asset, string $assessment_type, array $assessment_data = [], array $assessment_data_2 = []) {

    switch ($assessment_type) {
      case "crop":
        $results = $this->cftClient->cropCalculate($assessment_data);

        // Status Error will be return.
        if (is_numeric($results)) {
          return 'Crop Product has an error status of: ' . $results;
        }

        $results_water = $this->cftClient->waterCalculate($assessment_data_2);

        // Status Error for Water will be return.
        if (is_numeric($results_water)) {
          return 'Water Product has an error status of: ' . $results_water;
        }

        $assessment_id = $this->getAssessmentId($assessment_type, $asset->id());

        $summary = $results[0]['summary'];
        $total_emissions = $results[0]['total_emissions'];
        $query_crop = $this->database->insert('farm_cft_assessment_crop');
        $query_crop->fields([
          'assessment_id' => $assessment_id,
          'emissions_total' => implode(" ", $summary['emissions_total']),
          'emissions_per_area' => implode(" ", $summary['emissions_per_area']),
          'emissions_per_product' => implode(" ", $summary['emissions_per_product']),
          'soil_organic_carbon' => $summary['soil_organic_carbon'],
          'machinery_fuel_usage' => $summary['machinery_fuel_usage'],
        ]);
        $query_crop->execute();

        $query_total_emissions = $this->database->insert('farm_cft_assessment_total_emissions');
        $query_total_emissions->fields([
          'assessment_id',
          'name',
          'co2',
          'n2o',
          'ch4',
          'total_co2e',
          'total_co2e_per_area',
          'total_co2e_per_product',
        ]);
        foreach ($total_emissions as $total_emission) {
          $query_total_emissions->values([
            'assessment_id' => $assessment_id,
            'name' => $total_emission['name'],
            'co2' => (float) $total_emission['CO2'],
            'n2o' => (float) $total_emission['N2O'],
            'ch4' => (float) $total_emission['CH4'],
            'total_co2e' => (float) $total_emission['total_CO2e'],
            'total_co2e_per_area' => (float) $total_emission['total_CO2e_per_area'],
            'total_co2e_per_product' => (float) $total_emission['total_CO2e_per_product'],
          ]);
        }
        $query_total_emissions->execute();

        // Water Query.
        $water_summary = $results_water[0]['summary'];
        $water_footprint = $water_summary['water_footprint'];
        $water_balance = $water_summary['water_balance'];

        $query_water = $this->database->insert('farm_cft_assessment_water');
        $query_water->fields([
          'assessment_id' => $assessment_id,
          'water_productivity' => implode(" ", $water_summary['water_productivity']),
          'water_footprint_total' => $water_footprint['value'],
          'water_footprint_green' => (float) $water_footprint['green'],
          'water_footprint_blue' => (float) $water_footprint['blue'],
          'irrigation_efficiency' => implode(" ", $water_summary['irrigation_efficiency']),
          'water_balance_unit' => $water_balance['unit'],
          'water_balance_water_requirement' => implode(" ", $water_balance['water_requirement']),
          'water_balance_water_added' => implode(" ", $water_balance['water_added']),
          'irrigation_percolation' => (float) $water_balance['irrigation_percolation'],
          'irrigation_runoff' => (float) $water_balance['irrigation_runoff'],
          'rainfall_runoff' => (float) $water_balance['rainfall_runoff'],
          'rainfall_percolation' => (float) $water_balance['rainfall_percolation'],
          'irrigation_balance' => (float) $water_balance['irrigation_balance'],
          'interception_losses' => (float) $water_balance['interception_losses'],
        ]);
        $query_water->execute();
        break;

      case "dairy":
        $results = $this->cftClient->dairyCalculate($assessment_data);

        // Status Error will be returned.
        if (is_numeric($results)) {
          return 'Dairy Product has an error status of: ' . $results;
        }

        $summary = $results[0]['summary'];
        $total_emissions = $results[0]['total_emissions'];
        $assessment_id = $this->getAssessmentId($assessment_type, $asset->id());

        $query_dairy = $this->database->insert('farm_cft_assessment_dairy');
        $query_dairy->fields([
          'assessment_id' => $assessment_id,
          'emissions_total' => implode(" ", $summary['emissions_total']),
          'emissions_per_fpcm' => implode(" ", $summary['emissions_per_fpcm']),
        ]);
        $query_dairy->execute();

        $query_total_emissions_dairy = $this->database->insert('farm_cft_assessment_total_emissions_dairy');
        foreach ($total_emissions as $total_emission) {
          $query_total_emissions_dairy->values([
            'assessment_id' => $assessment_id,
            'name' => $total_emission['name'],
            'co2' => (float) $total_emission['CO2'],
            'n2o' => (float) $total_emission['N2O'],
            'ch4' => (float) $total_emission['CH4'],
            'co2e' => (float) $total_emission['total_CO2e'],
            'co2e_per_fpcm' => (float) $total_emission['total_CO2e_per_fpcm'],
          ]);
        }
        $query_total_emissions_dairy->execute();
        break;

      case "energy":
        $results = $this->cftClient->energyCalculate($assessment_data);

        // Status Error will be returned.
        if (is_numeric($results)) {
          return 'Energy Product has an error status of: ' . $results;
        }

        $assessment_id = $this->getAssessmentId($assessment_type, $asset->id());

        $query_energy = $this->database->insert('farm_cft_assessment_energy');
        $query_energy->fields([
          'assessment_id' => $assessment_id,
          'energy' => (float) $results[0]['energy'],
        ]);

        $query_energy->execute();
        break;
    }

    return $assessment_id;
  }

  /**
   * Returns Assessment ID.
   */
  private function getAssessmentId(string $type, string $id) {
    // Insert Assessment on every submit.
    $query_assessment = $this->database->insert('farm_cft_assessment');
    $query_assessment->fields(['assessment_type', 'asset_id', 'timestamp']);
    $query_assessment->values([$type, $id, $this->time->getCurrentTime()]);

    $assessment_id = $query_assessment->execute();

    return $assessment_id;
  }

}
