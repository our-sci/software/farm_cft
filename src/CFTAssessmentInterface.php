<?php

namespace Drupal\farm_cft;

use Drupal\asset\Entity\AssetInterface;

/**
 * An interface for the CFTAssessment service.
 */
interface CFTAssessmentInterface {

  /**
   * Create an estimate for the given Asset and specified project types.
   *
   * @param \Drupal\asset\Entity\AssetInterface $asset
   *   The asset to create the assessment.
   * @param string $assessment_type
   *   An string of assessment type.
   * @param array $assessment_data
   *   Assessment data.
   * @param array $assessment_data_2
   *   Assessment data.
   *
   * @return int|string
   *   The base assessment ID or FALSE if the assessment could not be created.
   */
  public function createAssessment(AssetInterface $asset, string $assessment_type, array $assessment_data = [], array $assessment_data_2 = []);

}
