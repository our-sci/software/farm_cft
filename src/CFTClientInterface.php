<?php

namespace Drupal\farm_cft;

use GuzzleHttp\ClientInterface;

/**
 * Interface for the CFT client.
 */
interface CFTClientInterface extends ClientInterface {

  /**
   * Returns the Crop Calculate data.
   *
   * @param array $assessment_data
   *   The assessment body.
   */
  public function cropCalculate(array $assessment_data);

  /**
   * Returns the Crop Calculate data.
   *
   * @param array $assessment_data
   *   The assessment body.
   */
  public function waterCalculate(array $assessment_data);

  /**
   * Returns the Crop Calculate data.
   *
   * @param array $assessment_data
   *   The assessment body.
   */
  public function dairyCalculate(array $assessment_data);

  /**
   * Returns the Energy Calculate data.
   *
   * @param array $assessment_data
   *   The assessment body.
   */
  public function energyCalculate(array $assessment_data);

  /**
   * Helper function to ping the CFT server.
   *
   * @return bool
   *   Boolean indicating success.
   */
  public function ping(): bool;

}
