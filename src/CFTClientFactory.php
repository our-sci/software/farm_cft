<?php

namespace Drupal\farm_cft;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Factory class to get authenticated instance of the CFTClient.
 */
class CFTClientFactory {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor for the CFTClientFactory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Returns a CFTClient authenticated with the config api_key and api_app_key.
   *
   * @return \Drupal\farm_cft\CFTClientInterface
   *   The CFTClient.
   */
  public function getAuthenticatedClient() {
    $config = $this->configFactory->get('farm_cft.settings');
    $api_key = $config->get('api_key');
    $api_app_key = $config->get('api_app_key');
    return new CFTClient($api_key, $api_app_key);
  }

}
