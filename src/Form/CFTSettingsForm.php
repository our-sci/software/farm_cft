<?php

namespace Drupal\farm_cft\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * CFT settings form.
 */
class CFTSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'farm_cft.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'farm_cft_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Api & App key field.
    $config = $this->config(static::SETTINGS);
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Api Key'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['api_app_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter API App key'),
      '#default_value' => $config->get('api_app_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $api_key = $form_state->getValue('api_key');
    $api_app_key = $form_state->getValue('api_app_key');

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('api_key', $api_key)
      ->set('api_app_key', $api_app_key)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
