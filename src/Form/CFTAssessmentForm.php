<?php

namespace Drupal\farm_cft\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\farm_cft\CFTAssessmentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Assessment Form ID.
 */
class CFTAssessmentForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The CFT Assessment service.
   *
   * @var \Drupal\farm_cft\CFTAssessmentInterface
   */
  protected $cftAssessment;

  /**
   * Constructs a CFTAssessmentForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\farm_cft\CFTAssessmentInterface $cft_assessment
   *   The cft assessment service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CFTAssessmentInterface $cft_assessment) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cftAssessment = $cft_assessment;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('farm_cft.assessment'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'farm_cft_assessment';
  }

  /**
   * Assessment Data.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['asset'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Asset'),
      '#description' => $this->t('Asset Autocomplete'),
      '#target_type' => 'asset',
      '#selection_settings' => [
        'target_bundles' => ['plant'],
      ],
      '#required' => TRUE,
    ];

    $form['assessment_type'] = [
      '#title' => $this->t('Assessment Type'),
      '#type' => 'select',
      '#description' => $this->t('Select the type of Cool Farm Tool assessment to create.'),
      '#required' => TRUE,
      '#options' => [
        "crop" => $this->t('Crop'),
        "dairy" => $this->t('Dairy'),
        "energy" => $this->t('Energy'),
      ],
    ];

    $form['assessment_data'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter Data'),
      '#required' => TRUE,
    ];

    $form['assessment_data_2'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter Data 2'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Assessment'),
    ];

    return $form;
  }

  /**
   * Submit Form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $asset_id = $form_state->getValue('asset');
    $asset = $this->entityTypeManager->getStorage('asset')->load($asset_id);
    $assessment_type = $form_state->getValue('assessment_type');
    $assessment_data = $form_state->getValue('assessment_data');
    $assessment_data_2 = $form_state->getValue('assessment_data_2');
    $json = Json::decode($assessment_data);
    $json_2 = Json::decode($assessment_data_2);

    $assessment_result = $this->cftAssessment->createAssessment($asset, $assessment_type, $json, $json_2);

    // The Assessment Result should return an ID or an error if it returns one.
    if (is_numeric($assessment_result)) {
      $this->messenger()->addMessage($this->t("Data has been sent and saved."));

      // View page of the Assessment created.
      $view_page = "view.cft_assessment.$assessment_type";

      // Redirect to the CFT Assessment view page.
      $form_state->setRedirect($view_page);
    }
    else {
      $this->messenger()->addError($assessment_result);
    }
  }

}
